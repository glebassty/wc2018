// import "../node_modules/antd/dist/antd.css";

import React from "react";
import { ThemeProvider } from "styled-components";
import theme from "../src/base/theme";
import { addDecorator } from "@storybook/react";
import { Provider } from "react-redux";
import store from "../src/redux/store";
import "../src/base/fonts.less";
import "../src/base/antd.less";

addDecorator((story) => {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>{story()}</Provider>
    </ThemeProvider>
  );
});
