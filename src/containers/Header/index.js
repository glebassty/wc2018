import React from "react";
import styled from "styled-components";
import { Container, NavTeams, NavMain, UiIcon } from "../../components";
import { Row, Col } from "antd";
import { MenuOutlined, SearchOutlined } from "@ant-design/icons";
import { ReactComponent as logo } from "../../assets/images/logo/img_logo-main.svg";
import { h5 } from "../../base/mixins/typography";
import { useBreakpoint } from "styled-breakpoints/react-styled";
import { up, down } from "styled-breakpoints";
import {
  linearGradientPolished,
  radialGradientPolished,
} from "../../base/mixins/gradients";
import theme from "../../base/theme";
import { rgba } from "polished";

let blockHeight = "120px";

const Header = () => {
  const isXl = useBreakpoint(up("xl"));
  const isMd = useBreakpoint(up("md"));
  const isSm = useBreakpoint(down("sm"));
  return (
    <Wrapper>
      <Container>
        <Row className='row'>
          {!isSm && (
            <Col className='logo' flex={isXl ? "120px" : "60px"}>
              <UiIcon
                component={logo}
                style={isXl ? { fontSize: "100px" } : { fontSize: "50px" }}
              />
            </Col>
          )}
          <Col className='nav' flex='auto'>
            {isXl && (
              <Wrap className='nav-teams'>
                <NavTeams />
              </Wrap>
            )}
            <Wrap className='nav-main'>
              {isSm && <LogoType>WC RUSSIA 2018</LogoType>}
              {isMd && <NavMain />}
              <Icons>
                <Search>
                  <SearchOutlined
                    style={{
                      fontSize: "28px",
                      color: theme.color.gold_light,
                      position: "relative",
                      zIndex: 1,
                    }}
                  />
                </Search>
                <Burger>
                  <MenuOutlined
                    style={{
                      fontSize: "28px",
                      color: theme.color.gold_light,
                      position: "relative",
                      zIndex: 1,
                    }}
                  />
                </Burger>
              </Icons>
            </Wrap>
          </Col>
        </Row>
      </Container>
    </Wrapper>
  );
};

const Wrapper = styled.header`
  box-shadow: 0px 5px 10px 2px ${rgba(theme.color.box_shadow, 0.5)};
  background: ${linearGradientPolished(
    theme.color.red_light,
    theme.color.red_dark
  )};
  .nav {
    border-left: 1px solid ${(p) => p.theme.color.gold_light};

    border-right: 1px solid ${(p) => p.theme.color.gold_light};
  }
  .logo {
    display: flex;
    justify-content: center;
    align-items: center;

    height: ${blockHeight};

    ${down("lg")} {
      height: calc(${blockHeight} / 2);
    }

    background: ${linearGradientPolished(
      theme.color.red_light,
      theme.color.red_dark
    )};

    border-left: 1px solid ${(p) => p.theme.color.gold_light};
  }
`;

const Wrap = styled.div`
  display: flex;
  align-items: center;

  &.nav-teams {
    background-color: ${(p) => p.theme.color.background};
    height: calc(${blockHeight} / 2);
    justify-content: center;
  }
  &.nav-main {
    display: flex;
    justify-content: space-between;
    padding-left: 20px;
    padding-right: 20px;

    height: calc(${blockHeight} / 2);
    background: ${radialGradientPolished(
      theme.color.red_light,
      theme.color.red_dark
    )};
  }
`;

const LogoType = styled.div`
  ${h5};
  color: ${(p) => p.theme.color.gold_light};
`;

const Icons = styled.div`
  display: flex;
`;
const Burger = styled.div``;

const Search = styled.div`
  margin-right: 20px;
`;

export default Header;
