import React from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import { NavList, Container } from "../../components";
import { FOOTER } from "../../constants";
import { linearGradientPolished } from "../../base/mixins/gradients";
import theme from "../../base/theme";
import { rgba } from "polished";

const Footer = () => {
  return (
    <Wrapper>
      <Container>
        <Row
          gutter={[
            { xs: 20, sm: 20, md: 40, lg: 60 },
            { xs: 20, sm: 40, md: 60 },
          ]}
        >
          {FOOTER.map((item) => (
            <Col key={item.id} xs={24} sm={12} md={8} lg={6}>
              <NavList
                id={item.id}
                title={item.title}
                url={item.url}
                data={item.data}
              />
            </Col>
          ))}
        </Row>
      </Container>
    </Wrapper>
  );
};

const Wrapper = styled.footer`
  box-shadow: 0px 5px 10px 2px ${rgba(theme.color.box_shadow, 0.8)};
  padding: 40px 0 40px 0;
  background: ${linearGradientPolished(
    theme.color.background2,
    theme.color.brown_dark
  )};
`;

// TODO сделать мобильную версию страницы ++++

// TODO посмотреть еще раз пагинацию ++++

export default Footer;
