import React from "react";
import {
  CardPlayer,
  PlayerSort,
  Container,
  Pagination,
} from "../../../components";
import { Row, Col, Spin } from "antd";
import { fetchSquads } from "../../../redux/actions/fetchData";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import {
  setPositions,
  setTeams,
  setPlayer,
  setMatches,
  setOrderSort,
  setAge,
  setSortInner,
  setSortOutSide,
  setClubs,
} from "../../../redux/actions/filters";
import { setPage } from "../../../redux/actions/pagination";

const Main = () => {
  const { items } = useSelector(({ dataReducer }) => dataReducer);
  // console.log("главный items", items);

  const { isLoaded } = useSelector(({ dataReducer }) => dataReducer);
  const { page } = useSelector(({ paginationReducer }) => paginationReducer);
  const {
    position,
    team,
    player,
    matches,
    orderSort,
    age,
    sortInner,
    sortOutSide,
    club,
  } = useSelector(({ filtersReducer }) => filtersReducer);

  const dispatch = useDispatch(
    position,
    team,
    player,
    matches,
    orderSort,
    page,
    age,
    sortInner,
    sortOutSide,
    club
  );

  React.useEffect(() => {
    dispatch(
      fetchSquads(
        position,
        team,
        player,
        matches,
        orderSort,
        page,
        age,
        sortInner,
        sortOutSide,
        club
      )
    );
  }, [
    position,
    team,
    player,
    matches,
    orderSort,
    page,
    age,
    sortInner,
    sortOutSide,
    club,
  ]);

  const onSelectPosition = React.useCallback((data) => {
    dispatch(setPositions(data));
  }, []);

  const onSelectTeams = React.useCallback((data) => {
    dispatch(setTeams(data));
  }, []);

  const onSelectPlayer = React.useCallback((data) => {
    dispatch(setPlayer(data));
  }, []);

  const onSelectMatches = React.useCallback((data) => {
    dispatch(setMatches(data));
  }, []);
  const onSelectOrderSort = React.useCallback((data) => {
    dispatch(setOrderSort(data));
  }, []);

  const onSelectAge = React.useCallback((data) => {
    dispatch(setAge(data));
  }, []);

  const onSelectSortInner = React.useCallback((data) => {
    dispatch(setSortInner(data));
  }, []);

  const onSelectSortOutSide = React.useCallback((data) => {
    dispatch(setSortOutSide(data));
  }, []);

  const onSelectClubs = React.useCallback((data) => {
    dispatch(setClubs(data));
  }, []);

  const onSelectPage = React.useCallback((data) => {
    dispatch(setPage(data));
  }, []);

  // Pagination
  const defaultPageSize = 30;
  const [pagination, setPagination] = React.useState({
    minValue: 0,
    maxValue: 30,
  });
  const changePagination = (value) => {
    setPagination({
      minValue: (value - 1) * defaultPageSize,
      maxValue: value * defaultPageSize,
    });
  };

  return (
    <Wrapper>
      <Container>
        <Row gutter={[0, 30]}>
          <Col>
            <PlayerSort
              onSelectTeams={onSelectTeams}
              onSelectPosition={onSelectPosition}
              onSelectPlayer={onSelectPlayer}
              onSelectMatches={onSelectMatches}
              onSelectOrderSort={onSelectOrderSort}
              onSelectAge={onSelectAge}
              onSelectSortInner={onSelectSortInner}
              onSelectClubs={onSelectClubs}
              onSelectSortOutSide={onSelectSortOutSide}
            />
          </Col>
          <Col style={{ width: "100%" }}>
            <Row gutter={[10, 20]}>
              {isLoaded
                ? items
                    .slice(pagination.minValue, pagination.maxValue)
                    .map((item) => (
                      <Col xs={12} md={6} lg={4} key={item.id}>
                        <CardPlayer
                          number={item.number}
                          position={item.position}
                          player={item.player}
                          age={item.age}
                          matches={item.matches}
                          club={item.club}
                          playerUrl={item.playerUrl}
                          teamUrl={item.teamUrl}
                        />
                      </Col>
                    ))
                : Array(6)
                    .fill(0)
                    .map((_, index) => (
                      <Col xs={12} md={6} lg={4} key={index} className='spin'>
                        <Spin size='large' tip='Загрузка' />
                      </Col>
                    ))}
            </Row>
          </Col>
          <Col>
            {items && (
              <Pagination
                items={items}
                changePagination={changePagination}
                defaultPageSize={defaultPageSize}
                onSelectPage={onSelectPage}
              />
            )}
          </Col>
        </Row>
      </Container>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 20px 0 20px 0;
  .spin {
    height: 320px;
    justify-content: center;
    display: flex;
    align-items: center;
  }
`;

export default Main;
