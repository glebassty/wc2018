import React from "react";
import styled from "styled-components";
import { Container } from "../../components";

import { Row, Col } from "antd";

const Main = ({ title, children }) => {
  return (
    <Wrapper>
      <Container>
        <Row>
          <Col>
            <Title>{title}</Title>
          </Col>
        </Row>
        <Row>{children}</Row>
      </Container>
    </Wrapper>
  );
};

const Wrapper = styled.main``;
const Title = styled.div``;

export default Main;
