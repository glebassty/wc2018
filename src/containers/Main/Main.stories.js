import React from "react";
import { storiesOf } from "@storybook/react";
import Main from ".";

storiesOf("Containers", module).add("Main", () => {
  return <Main />;
});
