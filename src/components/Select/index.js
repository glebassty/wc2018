import React from "react";
import styled from "styled-components";
import { Select as SelectComponent } from "antd";
import "./select.less";
import { down } from "styled-breakpoints";
import { useBreakpoint } from "styled-breakpoints/react-styled";

const { Option } = SelectComponent;

const Select = ({ items, onSelectPosition, placeholder, width }) => {
  const isSm = useBreakpoint(down("sm"));
  return (
    <Wrapper>
      <SelectComponent
        style={{ width: width }}
        size={isSm ? "small" : "middle"}
        placeholder={placeholder}
        onChange={onSelectPosition}
        optionFilterProp='children'
        filterOption={(input, option) =>
          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
      >
        {items.map((option) => (
          <Option key={option.id} value={option.value}>
            {option.label}
          </Option>
        ))}
      </SelectComponent>
    </Wrapper>
  );
};

Select.defaultProps = { items: [{ name: 1 }, { name: 2 }] };

const Wrapper = styled.div``;

export default Select;
