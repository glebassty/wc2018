import React from "react";
import ContentLoader from "react-content-loader";
import styled from "styled-components";
import theme from "../../base/theme";

const Skeleton = (props) => (
  <Wrapper>
    <ContentLoader
      rtl
      speed={1}
      width={186}
      height={320}
      viewBox='0 0 186 320'
      backgroundColor={theme.color.red_dark}
      foregroundColor={theme.color.background2}
      {...props}
    >
      <rect x='19' y='109' rx='0' ry='0' width='0' height='8' />
      <rect x='0' y='0' rx='16' ry='16' width='186' height='320' />
    </ContentLoader>
  </Wrapper>
);

const Wrapper = styled.div``;

export default Skeleton;
