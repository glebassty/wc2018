import React from "react";
import { storiesOf } from "@storybook/react";
import Skeleton from ".";

storiesOf("Components", module).add("Skeleton", () => {
  return <Skeleton />;
});
