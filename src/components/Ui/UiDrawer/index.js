import React from 'react'
import { Drawer } from 'antd'
import styled from 'styled-components'

const UiDrawer = ({ children, ...props }) => {
    return (
        <Wrapper>
            <Drawer className="uidrawer" height="auto" {...props}>
                {children}
            </Drawer>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    width: 100%;
`
export default UiDrawer
