import React from "react";
import { storiesOf } from "@storybook/react";
import UiDrawer from ".";

const NAV = {
  name: "qwe",
};

storiesOf("Components|Ui/Drawer", module).add("UiDrawer", () => (
  <UiDrawer visible {...NAV} />
));
