import React from "react";
import styled from "styled-components";
import { Button } from "antd";

const UiButton = ({ children, ...props }) => {
  return (
    <Wrapper className='uibutton__wrapper'>
      <Button {...props}>{children}</Button>
    </Wrapper>
  );
};

const Wrapper = styled.div``;

export default UiButton;
