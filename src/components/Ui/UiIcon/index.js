import React from "react";
import styled from "styled-components";

import Icon from "@ant-design/icons";

const UiIcon = ({ ...props }) => {
  return (
    <Wrapper className='uiicon__wrapper'>
      <Icon {...props} />
    </Wrapper>
  );
};

const Wrapper = styled.i`
  path {
    /* fill: inherit;  */
    /* transition: fill 1s; */
  }

  &:hover {
    /* path {
      fill: ${(p) => p.hoverColor || "inherit"};
    } */
  }
`;

export default UiIcon;
