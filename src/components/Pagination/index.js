import React from "react";
import styled from "styled-components";
import { Pagination as AntPagination } from "antd";

const Pagination = ({
  items,
  changePagination,
  defaultPageSize,
  onSelectPage,
}) => {
  const ref = React.useRef(null);
  console.log(ref, "что в рефе");
  const onChange = (value) => {
    changePagination(value);
    onSelectPage(value);
  };
  return (
    <Wrapper>
      <AntPagination
        ref={ref}
        active={true}
        defaultPageSize={defaultPageSize}
        // TODO надо, чтобы defaultCurrent сбрасывалась на 1
        // Чтобы это убрать нужен редакс, который будет сбрасывать значение на 1
        defaultCurrent={1}
        total={items.length}
        showSizeChanger={false}
        showQuickJumper
        onChange={onChange}
        showTotal={(total) => `Найдено записей -  ${total}`}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div``;

export default Pagination;
