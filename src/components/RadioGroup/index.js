import React from "react";
import { Radio as AntRadio, Col, Row } from "antd";

const RadioGroup = ({ radios, onSelectRadio, span }) => {
  const [value, setValue] = React.useState(radios[0].value);

  const onChange = (e) => {
    setValue(e.target.value);
    onSelectRadio(e.target.value);
  };

  return (
    <AntRadio.Group onChange={onChange} value={value}>
      <Row>
        {radios.map((radio) => (
          <Col span={span}>
            <AntRadio key={radio.id} value={radio.value}>
              {radio.label}
            </AntRadio>
          </Col>
        ))}
      </Row>
    </AntRadio.Group>
  );
};

RadioGroup.defaultProps = {
  radios: [
    { id: 1, value: "desc", label: "По убыванию" },
    { id: 2, value: "asc", label: "По возрастанию" },
  ],
};

export default RadioGroup;
