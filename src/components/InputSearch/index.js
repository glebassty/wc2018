import React from "react";
import styled from "styled-components";
import { Button, Input, Space } from "antd";
import "./inputSearch.less";
import { down } from "styled-breakpoints";
import { useBreakpoint } from "styled-breakpoints/react-styled";
import { ClearOutlined } from "@ant-design/icons";

const { Search } = Input;

const InputSearch = ({ onSelectPlayer, placeholder, className }) => {
  const ref = React.useRef(null);

  const clearInput = (e) => {
    ref.current.handleReset(e);
  };

  const isSm = useBreakpoint(down("sm"));

  return (
    <Wrapper className={className}>
      <Button
        className='ant-btn-primary'
        onClick={clearInput}
        size={isSm ? "small" : "middle"}
      >
        <ClearOutlined />
      </Button>
      <Search
        size={isSm ? "small" : "middle"}
        placeholder={placeholder}
        allowClear
        enterButton
        onSearch={onSelectPlayer}
        ref={ref}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
`;

export default InputSearch;
