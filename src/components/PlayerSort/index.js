import React from "react";
import styled from "styled-components";
import {
  Cascader,
  Select,
  InputSearch,
  Slider,
  RadioGroup,
  Checkbox,
  Switch,
  Collapse,
} from "../../components";
import { POSITIONS, TEAMS, SORT, AGE, CLUBS } from "../../constants";
import { sendDataClub } from "../../func";
import { Row, Col, Divider } from "antd";
import { paragraph, h5, paragraph2 } from "../../base/mixins/typography";
import { down } from "styled-breakpoints";
import { useBreakpoint } from "styled-breakpoints/react-styled";

const PlayerSort = ({
  onSelectTeams,
  onSelectPosition,
  onSelectPlayer,
  onSelectMatches,
  onSelectOrderSort,
  onSelectAge,
  onSelectSortInner,
  onSelectSortOutSide,
  onSelectClubs,
}) => {
  const [sortInner, setSortInner] = React.useState("ageSort");
  const [sortInnerChecked, setSortInnerChecked] = React.useState(true);

  const [sortOutSide, setSortOutSide] = React.useState("sortThree");
  const [sortOutSideChecked, setSortOutSideChecked] = React.useState(true);

  // ВОПРОС что делать с этими переключателями. Возможно ли как-то по другому реализовать
  // несколько рабочих сортировок, не связанных друг с другом

  //

  const toggleInnerChecked = () => {
    setSortInnerChecked(!sortInnerChecked);
    if (!sortInnerChecked === true) {
      setSortInner("ageSort");
    } else {
      setSortInner("matchesSort");
    }
    onSelectSortInner(sortInner);
  };

  const toggleOutSideSort = () => {
    setSortOutSideChecked(!sortOutSideChecked);
    if (!sortOutSideChecked === true) {
      setSortOutSide("sortThree");
    } else {
      setSortOutSide("sortOne");
    }
    onSelectSortOutSide(sortOutSide);
  };

  const [checkboxResult, setCheckboxResult] = React.useState([]);

  React.useEffect(() => {
    onSelectClubs(sendDataClub(checkboxResult));
  }, [checkboxResult]);

  const isSm = useBreakpoint(down("sm"));
  const gutterBottom = isSm ? 20 : "";
  return (
    <Wrapper>
      <Row gutter={[20, gutterBottom]}>
        <Col xs={24} lg={18}>
          <Title>Сортировка 1</Title>
          <Switch
            checked={sortOutSideChecked}
            onChange={toggleOutSideSort}
            unCheckedChildren='Off'
            checkedChildren='On'
            size='small'
          />
          <Row gutter={[0, gutterBottom]}>
            <Col xs={24} lg={7}>
              <Text>Выберете команду и позицию игрока</Text>
              <Row justify={isSm ? "center" : ""}>
                <Col lg={12}>
                  <Cascader
                    items={TEAMS}
                    onSelectTeams={onSelectTeams}
                    placeholder='команда'
                    width={isSm ? "90px" : ""}
                  />
                </Col>
                <Col lg={12}>
                  <Select
                    items={POSITIONS}
                    onSelectPosition={onSelectPosition}
                    placeholder='позиция'
                    width={isSm ? "90px" : ""}
                  />
                </Col>
              </Row>
            </Col>
            {!isSm && (
              <Col span={1}>
                <Divider type='vertical' style={{ height: "100%" }} />
              </Col>
            )}
            <Col xs={24} lg={16}>
              <Row justify='center' gutter={[20, 20]}>
                <Col span={11}>
                  <div className='switch'>
                    <Switch
                      checked={sortInnerChecked}
                      onChange={toggleInnerChecked}
                      size='small'
                    />
                  </div>
                  <Text>Укажите количество сыгранных матчей за сборную</Text>
                  <Slider
                    onSelectSlider={onSelectMatches}
                    defaultValue={[0, 157]}
                    minValue={0}
                    maxValue={157}
                  />
                </Col>
                <Col span={2}>
                  <Divider type='vertical' style={{ height: "100%" }} />
                </Col>
                <Col span={11}>
                  <div className='switch'>
                    <Switch
                      checked={!sortInnerChecked}
                      onChange={toggleInnerChecked}
                      size='small'
                    />
                  </div>
                  <Text>Укажите возраст игрока</Text>
                  <RadioGroup
                    span={isSm ? 24 : 12}
                    onSelectRadio={onSelectAge}
                    radios={AGE}
                  />
                </Col>
                <Col>
                  <RadioGroup
                    span={12}
                    radios={SORT}
                    className='radio-group'
                    onSelectRadio={onSelectOrderSort}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        {!isSm && (
          <Col span={1}>
            <Divider type='vertical' style={{ height: "100%" }} />
          </Col>
        )}
        <Col xs={24} lg={5}>
          <Title>Сортировка 2</Title>
          <Text>Введите название клуба или фамилию игрока</Text>
          <InputSearch
            onSelectPlayer={onSelectPlayer}
            placeholder='ввод на латинице'
          />
        </Col>
      </Row>
      <Row>
        <Col span={24} className='sort-three'>
          <Title>Сортировка 3</Title>
          <Switch
            checked={!sortOutSideChecked}
            onChange={toggleOutSideSort}
            size='small'
            unCheckedChildren='Off'
            checkedChildren='On'
          />
          <Collapse
            showArrow={false}
            title='Поиск игроков по выбранным клубам'
            ghost={true}
            sortOutSideChecked={sortOutSideChecked}
          >
            <Checkbox
              setCheckboxResult={setCheckboxResult}
              items={CLUBS}
              onSelectCheckbox={onSelectClubs}
              span={isSm ? 12 : 6}
            />
          </Collapse>
        </Col>
      </Row>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  .switch {
    display: flex;
    justify-content: flex-end;
  }
  span {
    ${down("sm")} {
      ${paragraph2}
    }
  }
  .sort-three {
    ${down("sm")} {
      margin-top: 10px;
    }
  }

  /* ВОПРОС как задать стили шрифта для Cascader, с помощью брейкпоинтов? */
`;

const Title = styled.div`
  ${h5}
  margin-bottom: 5px;
  color: ${(p) => p.theme.color.red_dark};

  ${down("sm")} {
    ${paragraph}
  }
`;

const Text = styled.div`
  text-align: center;
  ${paragraph}
  margin-bottom: 5px;
  ${down("sm")} {
    ${paragraph2}
  }
`;

export default PlayerSort;
