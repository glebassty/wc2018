import React from "react";
import styled from "styled-components";
import { Checkbox as AntCheckbox, Row, Col } from "antd";

const Checkbox = ({ className, items, span, setCheckboxResult }) => {
  const [checkedList, setCheckedList] = React.useState([]);
  const [indeterminate, setIndeterminate] = React.useState(true);
  const [checkAll, setCheckAll] = React.useState(false);

  React.useEffect(() => {
    setCheckboxResult(checkedList);
  }, [checkedList]);

  const onChange = (list) => {
    setCheckedList(list);
    setIndeterminate(!!list.length && list.length < items.length);
    setCheckAll(list.length === items.length);
  };

  const onCheckAllChange = (e) => {
    setCheckedList(e.target.checked ? items.map((item) => item.value) : []);
    setIndeterminate(false);
    setCheckAll(e.target.checked);
  };
  return (
    <Wrapper className={className}>
      <AntCheckbox
        indeterminate={indeterminate}
        onChange={onCheckAllChange}
        checked={checkAll}
      >
        Выбрать все
      </AntCheckbox>
      <AntCheckbox.Group value={checkedList} onChange={onChange}>
        <Row>
          {items.map((item) => (
            <Col span={span}>
              <AntCheckbox value={item.value}>{item.label}</AntCheckbox>
            </Col>
          ))}
        </Row>
      </AntCheckbox.Group>
    </Wrapper>
  );
};

const Wrapper = styled.div``;

export default Checkbox;
