import React from "react";
import styled from "styled-components";
import { Switch as AntSwitch } from "antd";

const Switch = ({
  className,
  defaultChecked,
  checked,
  onChange,
  size,
  checkedChildren,
  unCheckedChildren,
}) => {
  return (
    <Wrapper className={className}>
      <AntSwitch
        onChange={onChange}
        defaultChecked={defaultChecked}
        checked={checked}
        size={size}
        checkedChildren={checkedChildren}
        unCheckedChildren={unCheckedChildren}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div``;

export default Switch;
