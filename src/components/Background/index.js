import React from "react";
import styled from "styled-components";
import theme from "../../base/theme";

const Background = ({ color = theme.color.background }) => {
  return <Wrapper color={color} />;
};
const Wrapper = styled.div`
  z-index: -1;
  ::before {
    background: ${(p) => p.color};
    content: "";
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: -1;
  }
`;
export default Background;
