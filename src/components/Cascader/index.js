import React from "react";
import { Cascader } from "antd";
import "./cascader.less";
import { down } from "styled-breakpoints";
import { useBreakpoint } from "styled-breakpoints/react-styled";

function displayRender(label) {
  return label[label.length - 1];
}

const CascaderComponent = ({ items, onSelectTeams, placeholder, width }) => {
  const isSm = useBreakpoint(down("sm"));
  return (
    <Cascader
      size={isSm ? "small" : "middle"}
      placeholder={placeholder}
      options={items}
      expandTrigger='hover'
      onChange={onSelectTeams}
      autoFocus={true}
      displayRender={displayRender}
      style={{ width: width }}
    />
  );
};

export default CascaderComponent;
