import React from "react";
import "../../base/fonts.less";
import "../../base/antd.less";
import { LayoutBase } from "../../layouts";
import { MainPlayers } from "../../containers";

const App = () => {
  return (
    <>
      <LayoutBase>
        <MainPlayers />
      </LayoutBase>
    </>
  );
};

export default App;
