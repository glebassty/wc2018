import React from "react";
import { Slider as AntSlider } from "antd";

const Slider = ({ onSelectSlider, minValue, maxValue, defaultValue }) => {
  return (
    <AntSlider
      range={{ draggableTrack: true }}
      defaultValue={defaultValue}
      onChange={onSelectSlider}
      min={minValue}
      max={maxValue}
    />
  );
};

export default Slider;
