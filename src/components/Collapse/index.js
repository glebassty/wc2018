import React from "react";
import styled from "styled-components";
import { Collapse as AntCollapse } from "antd";

const { Panel } = AntCollapse;

function callback(key) {
  console.log(key);
}

const Collapse = ({
  children,
  title,
  bordered,
  ghost,
  sortOutSideChecked,
  showArrow,
}) => {
  console.log(!sortOutSideChecked, "sortOutSideChecked");

  // ВОПРОС как извлечь с помощью useRef фунцию ответственную  за открывание панели
  // ибо тут я подцепился за key
  // TODO сделать так, чтобы при нажатиии коллапс не открывался.

  return (
    <Wrapper>
      <AntCollapse
        defaultActiveKey={true}
        onChange={callback}
        bordered={bordered}
        ghost={ghost}
      >
        <Panel header={title} key={!sortOutSideChecked} showArrow={showArrow}>
          {children}
        </Panel>
      </AntCollapse>
    </Wrapper>
  );
};

const Wrapper = styled.div``;

export default Collapse;
