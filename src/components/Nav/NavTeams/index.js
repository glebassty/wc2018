import React from "react";

import styled from "styled-components";
import { NAVTEAMS } from "../../../constants";

const NavTeams = () => {
  return (
    <Wrapper>
      <Wrap>
        {NAVTEAMS.map((item) => {
          return (
            <a href={item.url} key={item.id}>
              <Block>
                <Image src={item.imageUrl} />
              </Block>
            </a>
          );
        })}
      </Wrap>
    </Wrapper>
  );
};

const Wrapper = styled.div``;
const Wrap = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
const Block = styled.div`
  padding-left: 6px;
  padding-bottom: 6px;
  :hover {
    transform: scale(1.3);
  }
  transition: all 0.1s ease-in-out;
`;
const Image = styled.img`
  max-width: 26px;
`;

export default NavTeams;
