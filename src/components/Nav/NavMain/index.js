import React from "react";
import styled from "styled-components";
import { h4, h5 } from "../../../base/mixins/typography";

import { down } from "styled-breakpoints";
import { NAVMAIN } from "../../../constants";

const NavMain = () => {
  return (
    <Wrapper>
      {NAVMAIN.map((item) => (
        <Item>
          <a key={item.id} href={item.url}>
            {item.name}
          </a>
        </Item>
      ))}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  ${h4};

  ${down("md")} {
    ${h5};
  }

  display: flex;
  a {
    color: ${(p) => p.theme.color.gold_light};
  }
  font-family: ${(p) => p.theme.fonts.family.primary};
`;

const Item = styled.div`
  padding-right: 20px;
`;

export default NavMain;
