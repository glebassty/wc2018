import React from "react";
import { storiesOf } from "@storybook/react";
import NavMain from ".";

const props = {
  data: [
    {
      id: 1,
      name: "Участники",
      url: "#",
    },
    {
      id: 2,
      name: "Игроки",
      url: "#",
    },
    {
      id: 3,
      name: "Турнирная таблица",
      url: "#",
    },
    {
      id: 4,
      name: "Статистика",
      url: "#",
    },
  ],
};

storiesOf("Components", module).add("NavMain", () => {
  return <NavMain {...props} />;
});
