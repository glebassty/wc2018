import React from "react";
import { storiesOf } from "@storybook/react";
import NavList from ".";

const DATA = {
  id: 1,
  title: "Участники",
  url: "#",
  data: [
    { id: 1, name: "Европа (УЕФА)", url: "#" },
    { id: 2, name: "Азия (АФК)", url: "#" },
    { id: 3, name: "Океания (ОФК)", url: "#" },
    { id: 4, name: "Африка (КАФ)", url: "#" },
    { id: 5, name: "Северная Америка (КОНКАКАФ)", url: "#" },
    { id: 6, name: "Южная Америка (КОНМЕБОЛ)", url: "#" },
  ],
};

storiesOf("Components", module).add("NavList", () => {
  return <NavList {...DATA} />;
});
