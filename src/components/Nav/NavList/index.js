import React from "react";
import styled from "styled-components";
import { h5, paragraph, p_mobile } from "../../../base/mixins/typography";
import { down } from "styled-breakpoints";

const NavList = ({ title, data }) => {
  return (
    <Wrapper>
      <Title>{title}</Title>
      <List>
        {data.map((item) => (
          <div key={item.id}>
            <a href={item.url}>{item.name}</a>
          </div>
        ))}
      </List>
    </Wrapper>
  );
};

const Wrapper = styled.div``;
const Title = styled.div`
  ${h5};
  color: ${(p) => p.theme.color.red_dark};
  ${down("md")} {
    ${paragraph};
  }
`;
const List = styled.div`
  ${p_mobile};
  ${down("md")} {
    ${p_mobile};
  }
  font-family: ${(p) => p.theme.fonts.family.secondary};
  font-weight: 900;
  a {
    color: ${(p) => p.theme.color.red_dark};
  }
`;

export default NavList;
