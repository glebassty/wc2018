import React from "react";
import styled from "styled-components";
import { paragraph, paragraph2, h5 } from "../../../base/mixins/typography";
import { Row as AntRow, Col as AntCol } from "antd";
import { UiIcon } from "../../";
import { down, up } from "styled-breakpoints";
import { useBreakpoint } from "styled-breakpoints/react-styled";
import { ReactComponent as icn_wclogo } from "../../../assets/images/logo/icn_wclogo.svg";
import {
  radialGradientPolished,
  linearGradientPolished,
} from "../../../base/mixins/gradients";
import theme from "../../../base/theme";
import { rgba } from "polished";

const CardPlayer = ({
  teamUrl,
  playerUrl,
  matches,
  club,
  position,
  number,
  age,
  player,
}) => {
  const img_background =
    "https://storage.yandexcloud.net/wc2018/assets/img_bg-player.jpg";
  const img_profile =
    "https://storage.yandexcloud.net/wc2018/assets/img_profile.png";

  const isSm = useBreakpoint(down("sm"));
  const spanColFirst = isSm ? 5 : 4;
  const spanColSecond = isSm ? 19 : 20;

  return (
    <Wrapper>
      <Row>
        <Col span={spanColFirst} className='col-first'>
          <Content>
            <div>
              <div>
                <ImgTeam src={teamUrl} />
              </div>
              <div>{number}</div>
              <div>{position}</div>
              <div>
                {age}
                <br />
                <span>age</span>
              </div>
            </div>
            <div>
              <UiIcon component={icn_wclogo} style={{ fontSize: "30px" }} />
            </div>
          </Content>
        </Col>
        <Col span={spanColSecond} className='col-second'>
          <Background background={img_background}>
            <div>
              {playerUrl ? (
                <Image src={playerUrl} />
              ) : (
                <Image src={img_profile} className='img_profile' />
              )}
            </div>
          </Background>
          <Player>{player}</Player>
          <Info>
            <div>{"Матчи (сборная) – " + matches}</div>
            <div>{"Клуб – " + club}</div>
          </Info>
        </Col>
      </Row>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  ${down("sm")} {
    height: 270px;
  }

  /* ВОПРОС что делать с точной высотой */
  ${up("lg")} {
    height: 320px;
  }

  .img_profile {
    padding-top: 5px;
    max-width: 125px;
  }
`;
const Row = styled(AntRow)`
  box-shadow: 0px 5px 10px 2px ${rgba(theme.color.box_shadow, 0.2)};
  border-radius: 8px;

  width: 100%;
`;

const Player = styled.div`
  ${h5};
  ${down("sm")} {
    ${paragraph2};
  }
  overflow: hidden;

  padding: 2px;
  color: ${(p) => p.theme.color.gold_light};
  border-top: 1px solid ${(p) => p.theme.color.gold_light};
  border-bottom: 1px solid ${(p) => p.theme.color.gold_light};
  text-transform: uppercase;

  background: ${radialGradientPolished(
    theme.color.blue_light,
    theme.color.blue_dark
  )};
  text-align: center;
`;

const Col = styled(AntCol)`
  &.col-first {
    ${h5};
    border-top-left-radius: 8px;
    border-bottom-left-radius: 8px;

    border-right: 1px solid ${(p) => p.theme.color.gold_light};
    color: ${(p) => p.theme.color.gold_light};
    align-items: center;

    background: ${linearGradientPolished(
      theme.color.red_light,
      theme.color.red_dark
    )};
  }
  &.col-second {
    border-bottom-right-radius: 8px;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`;

const Content = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;

  justify-content: space-between;

  padding-top: 10px;
  white-space: pre-wrap;
  line-height: 1;
  text-align: center;

  div {
    ${down("sm")} {
      ${paragraph2};
    }
    ${paragraph};

    margin-bottom: 8px;
    span {
      ${down("sm")} {
        ${paragraph2};
      }
      ${paragraph};
    }
  }
`;

const Info = styled.div`
  ${paragraph};

  ${down("sm")} {
    ${paragraph2};
    white-space: normal;
  }

  flex: 1;
  background: ${(p) => p.theme.color.white};
  color: ${(p) => p.theme.color.text_blue};
  padding: 4px;

  border-bottom-right-radius: 8px;
`;

const Background = styled.div`
  border-top-right-radius: 8px;
  display: flex;
  justify-content: center;
  background-image: url(${(p) => p.background});
  background-size: cover;
`;

const Image = styled.img`
  max-width: 130px;
  width: 100%;
`;

const ImgTeam = styled.img`
  max-width: 22px;
`;

export default CardPlayer;
