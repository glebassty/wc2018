import React from "react";
import { storiesOf } from "@storybook/react";
import CardPlayer from ".";
import styled from "styled-components";
import { Row, Col } from "antd";

const DATA = {
  teamUrl: "https://storage.yandexcloud.net/wc2018/teams/australia.png",
  playerUrl:
    "https://futhead.cursecdn.com/static/img/18/players_worldcup/188033.png",
  team: "Egypt",
  group: "A",
  number: 1,
  position: "GK",
  player: "Essam El-Hadary (captain)",
  date_of_birth: "1973/01/15",
  age: 45,
  matches: 157,
  goals: 0,
  club: "Al-Taawoun",
  player_count: 1,
};

storiesOf("Components|Cards/CardPlayer", module).add("CardPlayer", () => {
  return (
    <Wrapper>
      <Row gutter={[20, 0]}>
        <Col xs={12} md={6} lg={4}>
          <CardPlayer {...DATA} />
        </Col>
        <Col xs={12} md={6} lg={4}>
          <CardPlayer {...DATA} />
        </Col>
        <Col xs={12} md={6} lg={4}>
          <CardPlayer {...DATA} />
        </Col>
        <Col xs={12} md={6} lg={4}>
          <CardPlayer {...DATA} />
        </Col>
        <Col xs={12} md={6} lg={4}>
          <CardPlayer {...DATA} />
        </Col>
      </Row>
    </Wrapper>
  );
});

const Wrapper = styled.div`
  display: flex;
`;
