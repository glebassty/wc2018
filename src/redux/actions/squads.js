export const setSquads = (items) => ({
  type: "SET_SQUADS",
  payload: items,
});

export const setLoaded = (payload) => ({
  type: "SET_LOADED",
  payload,
});
