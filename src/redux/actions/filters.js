export const setPositions = (data) => ({
  type: "SET_POSITIONS",
  payload: data,
});

export const setTeams = (data) => ({
  type: "SET_TEAMS",
  payload: data,
});

export const setPlayer = (data) => ({
  type: "SET_PLAYER",
  payload: data,
});

export const setMatches = (data) => ({
  type: "SET_MATCHES",
  payload: data,
});

export const setOrderSort = (data) => ({
  type: "SET_ORDER_SORT",
  payload: data,
});

export const setAge = (data) => ({
  type: "SET_AGE",
  payload: data,
});

export const setSortOutSide = (data) => ({
  type: "SET_SORT_OUTSIDE",
  payload: data,
});
export const setSortInner = (data) => ({
  type: "SET_SORT_INNER",
  payload: data,
});
export const setClubs = (data) => ({
  type: "SET_CLUB",
  payload: data,
});
