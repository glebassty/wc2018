import axios from "axios";
import { setSquads } from "./squads";

const serverAddress = "https://w2018cup.herokuapp.com/squads";
const showTeams = (team) => {
  if (team && team.length === 2) {
    return team[1];
  } else {
    return team;
  }
};

export const fetchSquads = (
  position,
  team,
  player,
  matches,
  orderSort,
  page,
  age,
  sortInner,
  sortOutSide,
  club
) => (dispatch) => {
  console.log(page, "текущая страница редакс");

  dispatch({
    type: "SET_LOADED",
    payload: false,
  });

  dispatch({
    type: "SET_PAGE",
    payload: 1,
  });

  if (player) {
    return axios
      .get(`${serverAddress}?player_contains=${player}&_limit=-1`)
      .then(({ data }) => {
        dispatch(setSquads(data));
      });
  }

  if (sortOutSide === "sortThree") {
    return axios
      .get(`${serverAddress}?club_eq=${club}&_limit=-1`)
      .then(({ data }) => {
        dispatch(setSquads(data));
      });
  }

  if (sortOutSide === "sortOne" && sortInner === "ageSort") {
    return axios
      .get(
        `${serverAddress}?_sort=age:${orderSort}&age_gte=${age[0]}&age_lte=${
          age[1]
        }&position=${position}&team=${showTeams(team)}&_limit=-1`
      )
      .then(({ data }) => {
        dispatch(setSquads(data));
      });
  }
  if (sortOutSide === "sortOne" && sortInner === "matchesSort") {
    return axios
      .get(
        `${serverAddress}?_sort=matches:${orderSort}&matches_gte=${
          matches[0]
        }&matches_lte=${matches[1]}&position=${position}&team=${showTeams(
          team
        )}&_limit=-1`
      )
      .then(({ data }) => {
        dispatch(setSquads(data));
      });
  }
};
