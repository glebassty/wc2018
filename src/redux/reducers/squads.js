const initialState = {
  items: [],
  isLoaded: false,
};

const squads = (state = initialState, action) => {
  if (action.type === "SET_SQUADS") {
    return {
      ...state,
      items: action.payload,
      isLoaded: true,
    };
  }
  if (action.type === "SET_LOADED") {
    return {
      ...state,
      isLoaded: action.payload,
    };
  }
  return state;
};

export default squads;
