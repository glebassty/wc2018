import { combineReducers } from "redux";

import dataReducer from "./squads";
import filtersReducer from "./filters";
import paginationReducer from "./pagination";

const rootReducer = combineReducers({
  dataReducer,
  filtersReducer,
  paginationReducer,
});

export default rootReducer;
