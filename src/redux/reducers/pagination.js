const initialState = {
  page: 1,
};

const pagination = (state = initialState, action) => {
  if (action.type === "SET_PAGE") {
    return {
      ...state,
      page: action.payload,
    };
  }
  return state;
};

export default pagination;
