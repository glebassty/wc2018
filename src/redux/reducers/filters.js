const initialState = {
  position: undefined,
  team: undefined,
  player: undefined,
  matches: [0, 157],
  orderSort: "desc",
  age: [19, 45],
  sortInner: "matchesSort",
  sortOutSide: "sortOne",
  club: undefined,
};

const filters = (state = initialState, action) => {
  if (action.type === "SET_POSITIONS") {
    return {
      ...state,
      position: action.payload,
    };
  }
  if (action.type === "SET_TEAMS") {
    return {
      ...state,
      team: action.payload,
    };
  }
  if (action.type === "SET_PLAYER") {
    return {
      ...state,
      player: action.payload,
    };
  }
  if (action.type === "SET_MATCHES") {
    return {
      ...state,
      matches: action.payload,
    };
  }
  if (action.type === "SET_ORDER_SORT") {
    return {
      ...state,
      orderSort: action.payload,
    };
  }
  if (action.type === "SET_AGE") {
    return {
      ...state,
      age: action.payload,
    };
  }
  if (action.type === "SET_SORT_INNER") {
    return {
      ...state,
      sortInner: action.payload,
    };
  }
  if (action.type === "SET_SORT_OUTSIDE") {
    return {
      ...state,
      sortOutSide: action.payload,
    };
  }
  if (action.type === "SET_CLUB") {
    return {
      ...state,
      club: action.payload,
    };
  }
  return state;
};

export default filters;
