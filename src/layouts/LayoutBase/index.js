import React from "react";
import styled from "styled-components";
import { Header, Footer } from "../../containers";
import { Background } from "../../components";

const LayoutBase = ({ children }) => {
  return (
    <Wrapper>
      <Background />
      <Header />
      <main>{children}</main>
      <Footer />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  /* ВОПРОС обязателен ли тут min-height */
  min-height: 100vh;

  main {
    flex: 1;
  }
`;

export default LayoutBase;
