const sendDataClub = (value) => {
  if (value && value.length > 0) {
    const c = value.map((item) => "&club=" + item);
    const d = c.join("");
    const el = d.slice(6);
    return el;
  }
};

export default sendDataClub;
