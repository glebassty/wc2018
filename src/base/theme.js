const theme = {
  color: {
    blue_dark: "#074588",
    blue_light: "#0f80ca",

    gold_dark: "#C4AF7B",
    gold_light: "#F1EABF",

    brown_dark: "#D7D0C6",
    brown_light: "#DFD8CE",

    red_dark: "#7D0104",
    red_light: "#CF0404",

    background: "#f2ebe1",
    background2: "#e6dfd5",

    black: "#000",
    white: "#fff",

    text_black: "#423F46",
    text_blue: "#144a8b",

    box_shadow: "#1e3c50",
  },
  fonts: {
    family: {
      primary: "Dusha V5",
      secondary: "Mont",
      third: "DIN Pro Cond",
    },
    size: {
      h1: "3.052rem",
      h2: "2.441rem",
      h3: "1.953rem",
      h4: "1.563rem",
      h5: "1.25rem",
      p: "1rem",
      p2: "0.8rem",
      p_mobile: "0.64rem",
      p_mobile_s: "0.512 rem",
    },
  },
};

export default theme;
