import { linearGradient, radialGradient, rgba } from "polished";

const linearGradientPolished = (colorFirst, colorSecond) =>
  linearGradient({
    colorStops: [`${colorFirst} 0%`, `${colorSecond} 50%`],
    toDirection: "to bottom ",
    fallback: "#FFF",
  });

const radialGradientPolished = (colorFirst, colorSecond) =>
  radialGradient({
    colorStops: [`${colorFirst} 0%`, `${colorSecond} 100%`],
  });

export { linearGradientPolished, radialGradientPolished };
