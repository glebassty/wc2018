const Positions = [
  {
    id: 1,
    value: "GK",
    label: "🟤 GK",
  },
  {
    id: 2,
    value: "DF",
    label: "🔵 DF",
  },
  {
    id: 3,
    value: "MF",
    label: "🟢 MF",
  },
  {
    id: 4,
    value: "FW",
    label: "🔴 FW",
  },
  {
    id: 5,
    value: "FW&position=MF&position=DF&position=GK",
    label: "⚽ Все",
  },
];
export default Positions;
