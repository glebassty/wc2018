const NAVTEAMS = [
  {
    id: 1,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/argentina.png",
    url: "#",
  },
  {
    id: 2,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/australia.png",
    url: "#",
  },
  {
    id: 3,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/brazil.png",
    url: "#",
  },
  {
    id: 4,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/belgium.png",
    url: "#",
  },
  {
    id: 5,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/colombia.png",
    url: "#",
  },
  {
    id: 6,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/costa_rica.png",
    url: "#",
  },
  {
    id: 7,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/croatia.png",
    url: "#",
  },
  {
    id: 8,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/denmark.png",
    url: "#",
  },
  {
    id: 9,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/egypt.png",
    url: "#",
  },
  {
    id: 10,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/england.png",
    url: "#",
  },
  {
    id: 11,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/france.png",
    url: "#",
  },
  {
    id: 12,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/germany.png",
    url: "#",
  },
  {
    id: 13,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/iceland.png",
    url: "#",
  },
  {
    id: 14,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/iran.png",
    url: "#",
  },
  {
    id: 15,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/japan.png",
    url: "#",
  },
  {
    id: 16,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/korea_republic.png",
    url: "#",
  },
  {
    id: 17,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/mexico.png",
    url: "#",
  },
  {
    id: 18,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/morocco.png",
    url: "#",
  },
  {
    id: 19,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/nigeria.png",
    url: "#",
  },
  {
    id: 20,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/panama.png",
    url: "#",
  },
  {
    id: 21,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/peru.png",
    url: "#",
  },
  {
    id: 22,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/poland.png",
    url: "#",
  },
  {
    id: 23,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/portugal.png",
    url: "#",
  },
  {
    id: 24,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/russia.png",
    url: "#",
  },
  {
    id: 25,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/saudi_arabia.png",
    url: "#",
  },
  {
    id: 26,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/senegal.png",
    url: "#",
  },
  {
    id: 27,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/serbia.png",
    url: "#",
  },
  {
    id: 28,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/spain.png",
    url: "#",
  },
  {
    id: 29,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/sweden.png",
    url: "#",
  },
  {
    id: 30,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/switzerland.png",
    url: "#",
  },
  {
    id: 31,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/tunisia.png",
    url: "#",
  },
  {
    id: 32,
    imageUrl: "https://storage.yandexcloud.net/wc2018/teams/uruguay.png",
    url: "#",
  },
];

export default NAVTEAMS;
