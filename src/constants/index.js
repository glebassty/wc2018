export { default as POSITIONS } from "./Positions";
export { default as TEAMS } from "./Teams";
export { default as NAVTEAMS } from "./NavTeams";
export { default as FOOTER } from "./Footer";
export { default as NAVMAIN } from "./NavMain";
export { default as SORT } from "./Sort";
export { default as AGE } from "./Age";
export { default as CLUBS } from "./Clubs";
