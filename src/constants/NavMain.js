const NAVMAIN = [
  {
    id: 1,
    name: "Участники",
    url: "#",
  },
  {
    id: 2,
    name: "Игроки",
    url: "#",
  },
  {
    id: 3,
    name: "Турнирная таблица",
    url: "#",
  },
  {
    id: 4,
    name: "Статистика",
    url: "#",
  },
];

export default NAVMAIN;
