const fetchAllTeams = () => {
  const allTeams = [
    "Argentina",
    "Australia",
    "Belgium",
    "Brazil",
    "Colombia",
    "Costa Rica",
    "Croatia",
    "Denmark",
    "Egypt",
    "England",
    "France",
    "Germany",
    "Iceland",
    "Iran",
    "Japan",
    "South Korea",
    "Mexico",
    "Morocco",
    "Nigeria",
    "Panama",
    "Peru",
    "Poland",
    "Portugal",
    "Russia",
    "Saudi Arabia",
    "Senegal",
    "Serbia",
    "Spain",
    "Sweden",
    "Switzerland",
    "Tunisia",
    "Uruguay",
  ];
  const c = allTeams.map((item) => "&team=" + item);
  const d = c.join("");
  return d.slice(6);
};
const TEAMS = [
  {
    value: "ad",
    label: "A - D",
    children: [
      {
        value: "Argentina",
        label: "🇦🇷 Argentina",
      },
      {
        value: "Australia",
        label: "🇦🇺 Australia",
      },
      {
        value: "Belgium",
        label: "🇧🇪 Belgium",
      },
      {
        value: "Brazil",
        label: "🇧🇷 Brazil",
      },
      {
        value: "Colombia",
        label: "🇨🇴 Colombia",
      },
      {
        value: "Costa Rica",
        label: "🇨🇷 Costa Rica",
      },
      {
        value: "Croatia",
        label: "🇭🇷 Croatia",
      },
      {
        value: "Denmark",
        label: "🇩🇰 Denmark",
      },
    ],
  },
  {
    value: "ek",
    label: "E - K",
    children: [
      {
        value: "Egypt",
        label: "󠁧󠁢󠁥🇪🇬 Egypt",
      },
      {
        value: "England",
        label: "🏴󠁧󠁢󠁥󠁮󠁧󠁿 England",
      },
      {
        value: "France",
        label: "🇫🇷 France",
      },
      {
        value: "Germany",
        label: "🇩🇪 Germany",
      },
      {
        value: "Iceland",
        label: "🇮🇸 Iceland",
      },
      {
        value: "Iran",
        label: "🇮🇷 Iran",
      },
      {
        value: "Japan",
        label: "🇯🇵 Japan",
      },
      {
        value: "South Korea",
        label: "🇰🇷 South Korea",
      },
    ],
  },
  {
    value: "lp",
    label: "L - P",
    children: [
      {
        value: "Mexico",
        label: "🇲🇽 Mexico",
      },
      {
        value: "Morocco",
        label: "🇲🇦 Morocco",
      },
      {
        value: "Nigeria",
        label: "🇳🇬 Nigeria",
      },
      {
        value: "Panama",
        label: "🇵🇦 Panama",
      },
      {
        value: "Peru",
        label: "🇵🇪 Peru",
      },
      {
        value: "Poland",
        label: "🇵🇱 Poland",
      },
      {
        value: "Portugal",
        label: "🇵🇹 Portugal",
      },
    ],
  },
  {
    value: "qz",
    label: "Q - Z",
    children: [
      {
        value: "Russia",
        label: "🇷🇺 Russia",
      },
      {
        value: "Saudi Arabia",
        label: "🇸🇦 Saudi Arabia",
      },
      {
        value: "Senegal",
        label: "🇸🇳 Senegal",
      },
      {
        value: "Serbia",
        label: "🇷🇸 Serbia",
      },
      {
        value: "Spain",
        label: "🇪🇸 Spain",
      },
      {
        value: "Sweden",
        label: "🇸🇪 Sweden",
      },
      {
        value: "Switzerland",
        label: "🇨🇭 Switzerland",
      },
      {
        value: "Tunisia",
        label: "🇹🇳 Tunisia",
      },
      {
        value: "Uruguay",
        label: "🇺🇾 Uruguay",
      },
    ],
  },
  {
    value: fetchAllTeams(),
    label: "Все команды",
  },
];

// TODO найти способ избавиться от fetchTeams
// ВОПРОС как запросить все результаты в графе команды, без написания доболнительных условий?

export default TEAMS;
