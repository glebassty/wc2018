const CLUBS = [
  { value: "Bayern Munich", label: "Bayern Munich" },
  { value: "Manchester City", label: "Manchester City" },
  { value: "Real Madrid", label: "Real Madrid" },
  { value: "Inter Milan", label: "Inter Milan" },
  { value: "Barcelona", label: "Barcelona" },
  { value: "Paris Saint-Germain", label: "PSG" },
  { value: "Manchester United", label: "Manchester United" },
  { value: "Chelsea", label: "Chelsea" },
  { value: "Atlético Madrid", label: "Atlético Madrid" },
  { value: "Juventus", label: "Juventus" },
  { value: "Liverpool", label: "Liverpool" },
  { value: "Porto", label: "Porto" },
  { value: "Ajax", label: "Ajax" },
  { value: "Tottenham Hotspur", label: "Tottenham Hotspur" },
  { value: "Roma", label: "Roma" },
  { value: "Lazio", label: "Lazio" },
  { value: "Zenit Saint Petersburg", label: "Zenit" },
  { value: "Krasnodar", label: "Krasnodar" },
  { value: "Spartak Moscow", label: "Spartak Moscow" },
  { value: "CSKA Moscow", label: "CSKA Moscow" },
];

export default CLUBS;
