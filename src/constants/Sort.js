const SORT = [
  { id: 1, value: "desc", label: "По убыванию" },
  { id: 2, value: "asc", label: "По возрастанию" },
];

export default SORT;
