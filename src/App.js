import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./base/fonts.less";
import "../src/base/antd.less";
import { LayoutBase } from "./layouts";
import { Checkbox } from "./components";
import { MainPlayers } from "./containers";

const App = () => {
  return (
    <>
      <LayoutBase>
        <Router>
          <Route path='/' component={MainPlayers} />
          <Route path='/two' component={Checkbox} />
        </Router>
      </LayoutBase>
    </>
  );
};

export default App;
